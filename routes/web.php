<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

// Route::get('/home', function() {
//     $data = array('user' => Auth::user()->name );
//     return view('home')->with($data);
// })->name('home');

Route::get('/users', 'UserController@index')->name('users');
Route::get('/users/suspended', 'UserController@suspended')->name('users.suspended');
Route::get('/users/{id}', 'UserController@profile')->name('users');

Route::get('/users/suspended/{id}', 'UserController@userSuspend')->name('users.suspend');
Route::get('/users/unsuspended/{id}', 'UserController@unsuspend')->name('users.unsuspend');
Route::get('/users/delete/{id}', 'UserController@destroy')->name('users.delete');

Route::get('/questions', 'QuestionController@index')->name('questions');
Route::get('/questions/report', 'QuestionController@reportQuestions')->name('questions.report');
Route::get('/questions/flag', 'QuestionController@flagQuestions')->name('questions.flag');
Route::get('/questions/delete/{id}', 'QuestionController@destroy')->name('questions.delete');
Route::get('question/flag/{id}', 'QuestionController@questionFlag')->name('question.flagged');
Route::get('question/unflag/{id}', 'QuestionController@unFlagQuestion')->name('question.unflagged');

Route::get('/answers', 'AnswerController@index')->name('answers');
Route::get('/answers/report', 'AnswerController@reportAnswer')->name('answers.report');
Route::get('/answers/flag', 'AnswerController@flagAnswer')->name('answers.flag');
Route::get('/answers/delete/{id}', 'AnswerController@destroy')->name('answers.delete');
Route::get('answers/flag/{id}', 'AnswerController@commentFlag')->name('answer.flagged');
Route::get('answers/unflag/{id}', 'AnswerController@unFlagComment')->name('answer.unflagged');

Route::get('/emails', 'AnswerController@emails')->name('emails');

Route::post('/category', 'UserController@category')->name('category');
Route::get('/category', 'UserController@categories')->name('category');