<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $answers = Comment::where('is_flagged',0)->select('content', 'no_of_upvote', 'no_of_downvote','created_at','id')->orderByDesc('created_at')->paginate(10);
        $data = array('answers' => $answers, 'user' => Auth::user()->name);
        return view('pages/answers/all-answers')->with($data);
    }

    public function emails()
    {
        $answers = DB::table('contact_us')->select('name', 'id', 'email','message', 'created_at')->orderByDesc('created_at')->paginate(10);
        $data = array('answers' => $answers, 'user' => Auth::user()->name);
        return view('pages/users/message')->with($data);
    }

    public function reportAnswer()
    {
        $answers = DB::table('reported_comments')
        ->rightJoin('comments', 'reported_comments.id_comment', '=', 'comments.id')
        ->select('comments.content', 'comments.no_of_upvote', 'comments.no_of_downvote', 'comments.id', 'reported_comments.message', 'comments.created_at')
        ->where('reported_comments.message', '!=', null)
        ->orderByDesc('created_at')->paginate(10);

        $data = array('answers' => $answers, 'user' => Auth::user()->name);
        return view('pages/answers/reported')->with($data);
    }

    public function flagAnswer()
    {
        $answers = Comment::select('content', 'no_of_upvote', 'no_of_downvote', 'id', 'created_at')->where('is_flagged', 1)->orderByDesc('created_at')->paginate(10);
        $data = array('answers' => $answers, 'user' => Auth::user()->name);
        return view('pages/answers/flag')->with($data);
    }

    //delete post
    public function destroy($id)
    {
       $mypost = Comment::find($id);
            $mypost->delete();
       return redirect()->route('answers.flag')->with('success', 'Answer has been deleted successfully!');
    }

    //susupend post
    public function commentFlag($id)
    {
        $user = Comment::find($id);
        $user->is_flagged = 1;
        $user->flagged_by = Auth::user()->id;
        $user->save();
        return redirect()->route('answers.flag')->with('success', 'User comment has been flagged successfully!');
    }

     //unsusupend post
   public function unFlagComment($id)
   {
    $user = Comment::find($id);
    $user->is_flagged = 0;
    $user->flagged_by = Auth::user()->id;
    $user->save();
       return redirect()->route('answers')->with('success', 'User comment has been unflagged successfully!');
   }

}
