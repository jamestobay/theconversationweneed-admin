<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $questions = DB::table('posts')->select('content', 'no_of_followers', 'no_of_comments', 'no_of_views')->orderByDesc('created_at')->paginate(10);
        $questions = Posts::where('is_flagged',0)->orderByDesc('created_at')->paginate(10);
        $data = array('questions' => $questions, 'user' => Auth::user()->name);
        return view('pages/questions/all-questions')->with($data);
    }

    public function flagQuestions()
    {
        $flag = Posts::select('content', 'no_of_followers', 'no_of_comments', 'no_of_views', 'id')->where('is_flagged', 1)->orderByDesc('created_at')->paginate(10);
        $data = array('questions' => $flag, 'user' => Auth::user()->name);
        return view('pages/questions/flag')->with($data);
    }

    public function reportQuestions()
    {
        $report =  DB::table('posts')
            ->join('reported_posts', 'reported_posts.id_post', '=', 'posts.id')
            ->select('posts.content', 'posts.no_of_comments', 'posts.id', 'reported_posts.message', 'posts.created_at')
            ->where('reported_posts.message', '!=', null)
            ->orderByDesc('created_at')->paginate(10);
        $data = array('questions' => $report, 'user' => Auth::user()->name);
        // dd($data);
        return view('pages/questions/reported')->with($data);
    }

    //delete post
    public function destroy($id)
    {
        $mypost = Posts::find($id);
        $mypost->delete();
        return redirect()->route('questions.flag')->with('success', 'Question has been deleted successfully!');
    }

    //susupend post
    public function questionFlag($id)
    {
        $user = Posts::find($id);
        $user->is_flagged = 1;
        $user->flagged_by = Auth::user()->id;
        $user->save();
        return redirect()->route('questions.flag')->with('success', 'User Posts has been flagged successfully!');
    }

     //unsusupend post
   public function unFlagQuestion($id)
   {
    $user = Posts::find($id);
    $user->is_flagged = 0;
    $user->flagged_by = Auth::user()->id;
    $user->save();
       return redirect()->route('questions')->with('success', 'User Posts has been unflagged successfully!');
   }
}
