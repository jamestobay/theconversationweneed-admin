<?php

namespace App\Http\Controllers;


use App\User;
use App\Admin;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

       // $alluser = DB::table('users')->select('name', 'email', 'phone', 'no_of_followers', 'following')->orderByDesc('created_at')->paginate(10);
       $alluser = Admin::orderBy('created_at', 'desc')->paginate(10);
        $data = array('allusers' => $alluser, 'user' => Auth::user()->name );
        return view('pages/users/all-users')->with($data);
    }

    public function suspended()
    {
        $sususer = Admin::where('suspended',1)->orderByDesc('created_at')->paginate(10);
        $data = array('allusers' => $sususer, 'user' => Auth::user()->name );
        return view('pages/users/suspended-users')->with($data);
    }

     //susupend post
     public function userSuspend($id)
     {
         $user = Admin::find($id);
         $user->suspended = 1;
         $user->save();
         return redirect()->route('users.suspended')->with('success', 'User has been suspended successfully!');
     }

      //unsusupend post
    public function unSuspend($id)
    {
        $user = Admin::find($id);
        $user->suspended = 0;
        $user->save();
        return redirect()->route('users.suspended')->with('success', 'User has been unsuspended successfully!');
    }

    //delete post
    public function destroy($id)
    {
        $mypost = Admin::find($id);
        $mypost->delete();
        return redirect()->route('users')->with('success', 'User has been deleted successfully!');
    }


    public function profile($id)
    {
        $profile = DB::table('users')->select('name', 'email', 'phone', 'no_of_followers', 'following')->where('id', $id)->first();
        $data = array('profile' => $profile, 'user' => Auth::user()->name );
        return view('pages/users/profile')->with($data);
    }




    public function category(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('/categories')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            Category::create([
                'name' => Input::get('name'),
                'description' => Input::get('description'),
                'created_by' => $request->user()->id,
            ]);

            return redirect('/category');
        } catch (\Exception $ex) {
            Log::error("UserController::eventAvatar()  " . $ex->getMessage());
        }
    }

    public function categories()
    {
        $answers = Category::orderByDesc('created_at')->paginate(10);
        $data = array('answers' => $answers, 'user' => Auth::user()->name );
        return view('pages/users/category')->with($data);
    }

}
