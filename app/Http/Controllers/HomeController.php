<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $noOfQuestions = DB::table('posts')->count();
        $noOfAnswers = DB::table('comments')->count();
        $noOfUsers = DB::table('users')->count();

        $data = array('noOfQuestions' => $noOfQuestions, 'noOfAnswers' => $noOfAnswers, 'noOfUsers' => $noOfUsers , 'user' => Auth::user()->name);
        
        return view('dashboard')->with($data);
    }
}
