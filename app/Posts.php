<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_category', 'content', 'no_of_followers', 'no_of_comments', 'no_of_expected_comments',
    ];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
