@extends('layouts.app')

@section('content')

<!-- navbar -->
<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="{{url('/')}}">TCWN</a>
    <a class="navbar-brand brand-logo-mini" href="{{url('/')}}" style="padding-left:5px;">TCWN</a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-stretch">
    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item nav-profile dropdown">
        <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          <div class="nav-profile-img">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4Z1fW_gR1IEAD-HHXZMnmNlbkhg0F5XHC7p5esaBJZ1UwS3aa" alt="image">
            <span class="availability-status online"></span>
          </div>
          <div class="nav-profile-text">
            <p class="mb-1 text-black"> {{$user}}</p>
          </div>
        </a>
        <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            <i class="mdi mdi-logout mr-2 text-primary"></i>
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-menu"></span>
    </button>
  </div>
</nav>



<!-- partial -->
<div class="container-fluid page-body-wrapper">

  <!-- partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-profile">
        <a href="#" class="nav-link">
          <div class="nav-profile-text d-flex flex-column">
            <span class="font-weight-bold mb-2"> {{$user}}</span>
          </div>
          <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('dashboard')}}">
          <span class="menu-title">Dashboard</span>
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-user" aria-expanded="false" aria-controls="ui-user">
          <span class="menu-title">Users</span>
          <i class="menu-arrow"></i>
          <i class="mdi mdi-crosshairs-gps menu-icon"></i>
        </a>
        <div class="collapse" id="ui-user">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{url('users')}}">All Users</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{url('users/suspended')}}">Suspended Users</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-questions" aria-expanded="false" aria-controls="ui-questions">
          <span class="menu-title">Questions</span>
          <i class="menu-arrow"></i>
          <i class="mdi mdi-crosshairs-gps menu-icon"></i>
        </a>
        <div class="collapse" id="ui-questions">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{url('questions')}}">All Questions</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{url('questions/flag')}}">Flagged Questions</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{url('questions/report')}}">Reported Questions</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-answers" aria-expanded="false" aria-controls="ui-answers">
          <span class="menu-title">Answers</span>
          <i class="menu-arrow"></i>
          <i class="mdi mdi-crosshairs-gps menu-icon"></i>
        </a>
        <div class="collapse" id="ui-answers">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{url('answers')}}">All Answers</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{url('answers/flag')}}">Flagged Answers</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{url('answers/report')}}">Reported Answers</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('emails')}}">
          <span class="menu-title">Emails</span>
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('category')}}">
          <span class="menu-title">Category</span>
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>
    </ul>
  </nav>





  <!-- partial -->
  <div class="main-panel">
    <div class="container">
      @if(Session::has('success'))
      <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
          <div id="message" class="alert alert-success">
            {{ Session::get('success') }}
          </div>
        </div>
      </div>
      @endif
    </div>


    @yield('dashboard')


    <!-- partial:partials/_footer.html -->
    <footer class="footer">
      <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © {{ date('Y') }} <p>theconversationweneed</p>. All rights reserved.</span>
      </div>
    </footer>
    <!-- partial -->
  </div>

</div>

@endsection