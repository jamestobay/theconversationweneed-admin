<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/css/vendor.bundle.base.css') }}" rel="stylesheet">

    <link href="{{ asset('images/favicon.png') }}" rel="shortcut icon">
</head>

<body>
    <div id="app">

        <main>
            <div class="container-scroller">
                @yield('content')
            </div>

        </main>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link href="{{ URL::asset('vendors/js/vendor.bundle.base.js') }}" rel="script">
    <link href="{{ URL::asset('vendors/js/vendor.bundle.addons.js') }}" rel="script">
    <link href="{{ URL::asset('js/off-canvas.js') }}" rel="script">
    <link href="{{ URL::asset('js/misc.js') }}" rel="script">
    <link href="{{ URL::asset('js/dashboard.js') }}" rel="script">
    <script type="text/javascript">
        (function($) {
            'use strict';
            $(function() {
                $('[data-toggle="offcanvas"]').on("click", function() {
                    $('.sidebar-offcanvas').toggleClass('active')
                });
            });
        })(jQuery);
    </script>
</body>

</html>