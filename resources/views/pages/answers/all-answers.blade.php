@extends('home')

@section('dashboard')
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">All Answers</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                #
                                </th>
                                <th>
                                    Answers
                                </th>
                                <th>
                                    No of Downvote(s)
                                </th>
                                <th>
                                   No of Upvote(s)
                                </th>
                                <th>
                                    date
                                </th>
                                <th>
                                  Action
                                </th>
                            </tr>
                        </thead>
                        @foreach ($answers as $answer) 
                        <tbody>
                            <tr>
                                <td>
                                {{ $loop->iteration }}
                                </td>
                                <td>
                                {{ $answer->content }}
                                </td>
                                <td>
                                {{ $answer->no_of_downvote }}
                                </td>
                                <td>
                                {{ $answer->no_of_upvote }}
                                </td>
                               <td>{{ $answer->created_at }}</td>
                               <td><a href="{{  url('answers/flag/'.$answer->id) }}" class="btn btn-info">Flag</a></td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {{ $answers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection