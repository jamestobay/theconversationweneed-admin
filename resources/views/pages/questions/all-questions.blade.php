@extends('home')

@section('dashboard')
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">All Questions</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                #
                                </th>
                                <th>
                                    Question
                                </th>
                                <th>
                                    No of Followers
                                </th>
                                <th>
                                    No of Comments
                                </th>
                                <th>
                                    Number of Views
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        @foreach ($questions as $question) 
                        <tbody>
                            <tr>
                                <td>
                                {{ $loop->iteration }}
                                </td>
                                <td>
                                {{ $question->content }}
                                </td>
                                <td>
                                {{ $question->no_of_followers }}
                                </td>
                                <td>
                                {{ $question->no_of_comments }}
                                </td>
                                <td>
                                {{ $question->no_of_views }}
                                </td>
                                <td><a href="{{  url('question/flag/'.$question->id) }}" class="btn btn-info">flag</a> </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {{ $questions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection