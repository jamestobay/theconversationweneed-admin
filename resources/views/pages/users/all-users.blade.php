@extends('home')

@section('dashboard')
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">All Users</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Phone number
                                </th>
                                <th>
                                    Number of followers
                                </th>
                                <th>
                                    Following
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        @foreach ($allusers as $alluser)
                        <tbody>

                            <tr>

                                <td>
                                {{ $loop->iteration }}
                                </td>
                                <td>
                                    {{ $alluser->name }}
                                </td>
                                <td>
                                    {{ $alluser->email }}
                                </td>
                                <td>
                                    {{ $alluser->phone }}
                                </td>
                                <td>
                                    {{ $alluser->no_of_followers }}
                                </td>
                                <td>
                                    {{ $alluser->following }}
                                </td>
                                <td><a href="{{ url('users/'.$alluser->id) }}" class="btn btn-secondary">View</a> <a href="{{  url('users/suspended/'.$alluser->id) }}" class="btn btn-info">Suspend</a></td>
                            </tr>

                        </tbody>
                        @endforeach
                    </table>
                    {{ $allusers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection