@extends('home')

@section('dashboard')
<div class="row">
    <div class="col-md-8 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Categories</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                #
                                </th>
                                <th>
                                   Title
                                </th>
                                <th>
                                   Description
                                </th>
                                <th>
                                    Date
                                </th>
                            </tr>
                        </thead>
                        @foreach ($answers as $answer) 
                        <tbody>
                            <tr>
                                <td>
                                {{ $loop->iteration }}
                                </td>
                                <td>
                                {{ $answer->name }}
                                </td>
                                <td>
                                {{ $answer->description }}
                                </td>
                                <td>{{ $answer->created_at }}</td>
                               <!-- <td> <a href="{{ route('answers.delete', ['id' => $answer->id]) }}" class="btn btn-danger btn-sm">Delete</a>  </td> -->
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {{ $answers->links() }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 grid-margin stretch-card">
    <div class="card">
            <div class="card-body">
                <h4 class="card-title">Add Categories</h4>

                    <form class="pt-3" method="POST" action="{{ route('category') }}">

                        @csrf
                        <div class="form-group">
                            <input id="name" type="text" class="form-control form-control-lg @error('text') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="name" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="description" type="text" class="form-control form-control-lg @error('description') is-invalid @enderror" name="description" placeholder="Description" >
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mt-3">
                            <button type="submit" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">
                                Add category
                            </button>
                        </div>
                    </form>
             

            </div>
    </div>
    </div>
</div>

@endsection