@extends('home')

@section('dashboard')
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Emails</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                #
                                </th>
                                <th>
                                   Name
                                </th>
                                <th>
                                   Email
                                </th>
                                <th>
                                   Message
                                </th>
                                <th>
                                    Date
                                </th>
                            </tr>
                        </thead>
                        @foreach ($answers as $answer) 
                        <tbody>
                            <tr>
                                <td>
                                {{ $loop->iteration }}
                                </td>
                                <td>
                                {{ $answer->name }}
                                </td>
                                <td>
                                {{ $answer->email }}
                                </td>
                                <td>
                                {{ $answer->message }}
                                </td>
                                <td>{{ $answer->created_at }}</td>
                               <!-- <td> <a href="{{ route('answers.delete', ['id' => $answer->id]) }}" class="btn btn-danger btn-sm">Delete</a>  </td> -->
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {{ $answers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection